package com.citi.training.products.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.products.Product;
import com.citi.training.products.dao.ProductDao;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTests {

	@Autowired
	ProductService productService;
	
	@MockBean
	private ProductDao mockProductDao;
	
	@Test
	public void test_createRuns() {
		int newId = 1;
		
		when(mockProductDao.create(any(Product.class))).thenReturn(newId);
		int createdId = productService.create(new Product(1, "Test", 9.99));
		
		assertEquals(newId,createdId);
	}
	
	@Test
	public void test_findById() {
		int testId = 66;
		
		Product testProduct = new Product(testId, "Beans", 100.25);
		
		when(mockProductDao.findById(testId)).thenReturn(testProduct);
		
		Product returnedProduct = productService.findById(testId);
		
		verify(mockProductDao).findById(testId);
		assertEquals(testProduct, returnedProduct);
	}
	
	@Test
	public void test_delete() {
		productService.deleteById(4);
		
		verify(mockProductDao).deleteById(4);
	}

}
