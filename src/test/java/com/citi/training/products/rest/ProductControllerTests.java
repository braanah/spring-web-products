package com.citi.training.products.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.products.Product;
import com.citi.training.products.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest
public class ProductControllerTests {

    private static final Logger logger =
                LoggerFactory.getLogger(ProductControllerTests.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService mockProductDao;

    @Test
    public void findAllProducts_returnsList() throws Exception {
        when(mockProductDao.findAll())
            .thenReturn(new ArrayList<Product>());

        MvcResult result = this.mockMvc
                .perform(get(ProductController.BASE_PATH))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber())
                .andReturn();

        logger.info("Result from productDao.findAll: "
                    + result.getResponse().getContentAsString());
    }

    @Test
    public void createProduct_returnsCreated() throws Exception {
        Product testProduct = new Product(1, "Jam", 29.99);

        when(mockProductDao
                .create(any(Product.class)))
            .thenReturn(testProduct.getId());

        this.mockMvc.perform(
                post(ProductController.BASE_PATH)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(testProduct)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id")
                    .value(testProduct.getId()))
                .andReturn();
        logger.info("Result from Create Product");
    }

    @Test
    public void deleteProduct_returnsOK() throws Exception {
        MvcResult result = this.mockMvc
                .perform(delete(ProductController.BASE_PATH + "/1"))
                .andExpect(status().isNoContent())
                .andReturn();

        logger.info("Result from productDao.delete: "
                    + result.getResponse().getContentAsString());
    }
}