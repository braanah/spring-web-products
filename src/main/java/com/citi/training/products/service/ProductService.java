package com.citi.training.products.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.citi.training.products.Product;
import com.citi.training.products.dao.ProductDao;

@Component
public class ProductService {
	@Autowired
	ProductDao productDao;

	public List<Product> findAll() {
		return productDao.findAll();
	}
	
	public int create(@RequestBody Product product) {
		return productDao.create(product);
	}
	
	public Product findById(@PathVariable int id) {
		return productDao.findById(id);
	}
	
	public void deleteById(@PathVariable int id) {
		productDao.deleteById(id);
	}
}
