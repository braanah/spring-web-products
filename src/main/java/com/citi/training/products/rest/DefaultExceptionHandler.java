package com.citi.training.products.rest;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.citi.training.products.exceptions.ProductNotFoundException;

@ControllerAdvice
@Priority(20)
public class DefaultExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @ExceptionHandler(value = {ProductNotFoundException.class})
    public ResponseEntity<Object> productNotFoundExceptionHandler(
            HttpServletRequest request, ProductNotFoundException ex) {
        logger.warn(ex.toString());
        return new ResponseEntity<>("{\"message\": \"" + ex.getMessage() + "\"}",
                                    HttpStatus.NOT_FOUND);
    }
}