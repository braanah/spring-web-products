package com.citi.training.products.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.products.Product;
import com.citi.training.products.service.ProductService;

@RestController
//@RequestMapping("/product")
public class ProductController {

	public URI BASE_PATH = "/product";
	@Autowired
	ProductService productService;

	@RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Product> findAll() {
		return productService.findAll();
	}

	@RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void create(@RequestBody Product product) {
		productService.create(product);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Product findById(@PathVariable int id) {
		return productService.findById(id);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteById(@PathVariable int id) {
		productService.deleteById(id);
	}

}
