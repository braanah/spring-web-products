package com.citi.training.products.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.products.Product;
import com.citi.training.products.dao.ProductDao;
import com.citi.training.products.exceptions.ProductNotFoundException;

@Component
public class MysqlProductDao implements ProductDao {

	@Autowired
	JdbcTemplate tpl;

	public List<Product> findAll() {
		return tpl.query("select * from product", new ProductMapper());
	}

	private static final class ProductMapper implements RowMapper<Product> {
		public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Product(rs.getInt("id"), rs.getString("name"), rs.getDouble("price"));
		}
	}

	
	public Product findById(int id) {
		List<Product> products = this.tpl.query("select id, name, price from product where id = ?",
				new Object[] { id }, new ProductMapper());
		if (products.size() <= 0) {
			throw new ProductNotFoundException("No product found with id: " + id);
		}
		return products.get(0);

	}

	public int create(Product product) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		this.tpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement("insert into product (name, price) values (?, ?)",
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, product.getName());
				ps.setDouble(2, product.getPrice());
				return ps;
			}
		}, keyHolder);
		return keyHolder.getKey().intValue();

	}

	@Override
	public void deleteById(int id) {
		this.tpl.update("delete from product where id=?", id);
	}

}
