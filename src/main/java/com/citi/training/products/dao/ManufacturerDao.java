package com.citi.training.products.dao;

import java.util.List;

import com.citi.training.products.model.Manufacturer;

public interface ManufacturerDao {
	
	List<Manufacturer> findAll();

	Manufacturer findById(int id);

	int create(Manufacturer product);

	void deleteById(int id);

}
