package com.citi.training.products.dao;

import java.util.List;

import com.citi.training.products.Product;

public interface ProductDao {
	List<Product> findAll();

	Product findById(int id);

	int create(Product product);

	void deleteById(int id);

}
