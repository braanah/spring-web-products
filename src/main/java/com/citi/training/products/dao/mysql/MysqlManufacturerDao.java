package com.citi.training.products.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.citi.training.products.model.Manufacturer;

public class MysqlManufacturerDao {

	@Autowired
	JdbcTemplate tpl;

	public List<Manufacturer> findAll() {
		return tpl.query("select * from manufacturer", new ManufacturerMapper());
	}

	private static final class ManufacturerMapper implements RowMapper<Manufacturer> {
		public Manufacturer mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Manufacturer(rs.getInt("id"), rs.getString("name"), rs.getString("address"));
		}
	}

	public Manufacturer findById(int id) {
		List<Manufacturer> mans = this.tpl.query("select id, name, address from manufacturer where id = ?",
				new Object[] { id }, new ManufacturerMapper());
		if (mans.size() <= 0) {
			return null;
		}
		return mans.get(0);

	}

	public int create(Manufacturer manufacturer) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		this.tpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						"insert into manufacturer (name, address) values (?, ?)", Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, manufacturer.getName());
				ps.setString(2, manufacturer.getAddress());
				return ps;
			}
		}, keyHolder);
		return keyHolder.getKey().intValue();
	}

	public void deleteById(int id) {
		this.tpl.update("delete from manufacturer where id=?", id);
	}
}
